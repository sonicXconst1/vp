# Vulkan playground

Simple Vulkan playground.

* [Tutorial](https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Base_code)

## TODO

1. Implement `Drop` for `Vulkan`!
2. Consider `swapchain` reset before implementation.
