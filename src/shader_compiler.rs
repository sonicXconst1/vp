pub struct ShaderCompiler {
    path_to_compiler_executable: std::path::PathBuf
}

impl ShaderCompiler {
    pub fn new(
        path_to_compiler_executable: std::path::PathBuf,
    ) -> ShaderCompiler {
        ShaderCompiler {
            path_to_compiler_executable
        }
    }

    pub fn compile(
        &self,
        path_to_shader: &std::path::Path,
        output: &std::path::Path,
    ) -> bool {
        let shader_file_type = match std::fs::metadata(path_to_shader) {
            Ok(metadata) => metadata.file_type(),
            Err(error) => {
                log::error!(
                    "Shader file does not exists {:?}: {:?}",
                    path_to_shader,
                    error);
                return false;
            },
        };
        if shader_file_type.is_dir() {
            log::error!(
                "Shader file is a directory: {:?}",
                path_to_shader);
            return false;
        }
        let mut child = match std::process::Command::new(
            &self.path_to_compiler_executable)
            .arg(path_to_shader)
            .arg("-o")
            .arg(output)
            .spawn() {
                Ok(child) => child,
                Err(error) => {
                    log::error!(
                        "Failed to spawn compiler process: {:?}",
                        error);
                    return false;
                }
        };
        let exit_status = match child.wait() {
            Ok(exit_status) => exit_status,
            Err(error) => {
                log::error!(
                    "Failed to wait on compiler: {:?}",
                    error);
                return false;
            },
        };
        if !exit_status.success() {
            log::warn!("Shader compilation failed: {:?}", exit_status);
            return false;
        }
        return true;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_shader_compilation() {
        let glsl_fragment_shader = std::path::Path::new("shaders/shader.vert");
        let glsl_vertex_shader = std::path::Path::new("shaders/shader.vert");

        let spv_fragment_shader = std::path::Path::new("shaders/frag.spv");
        let spv_vertex_shader = std::path::Path::new("shaders/vert.spv");

        let shader_compiler = ShaderCompiler::new(
            std::path::PathBuf::from("glslc.exe"));

        assert!(
            shader_compiler.compile(&glsl_fragment_shader, &spv_fragment_shader),
            "Failed to compile fragment shader!");
        assert!(
            shader_compiler.compile(&glsl_vertex_shader, &spv_vertex_shader),
            "Failed to compile vertex shader!");
    }
}
