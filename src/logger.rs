use log;

pub struct Logger;

impl Logger {
    pub fn init(&self) {
        log::set_max_level(log::LevelFilter::Trace)
    }
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args())
        }
    }

    fn flush(&self) {}
}
