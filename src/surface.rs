use ash;
use log;
use raw_window_handle;

pub fn create_surface(
    instance: &ash::Instance,
    entry: &ash::Entry,
    raw_window_handle: &dyn raw_window_handle::HasRawWindowHandle,
) -> Option<ash::vk::SurfaceKHR> {
    match raw_window_handle.raw_window_handle() {
        #[cfg(target_os = "linux")]
        raw_window_handle::RawWindowHandle::Xlib(xlib) => {
            log::info!("Found Xlib window handle: {:?}", xlib);
            get_xlib_surface(instance, entry, &xlib)
        }
        #[cfg(target_os = "linux")]
        raw_window_handle::RawWindowHandle::Xcb(xcb) => {
            log::info!("Found Xcb window handle: {:?}", xcb);
            unimplemented!("Xcb windows are not implemented yet!")
        }
        #[cfg(target_os = "windows")]
        raw_window_handle::RawWindowHandle::Windows(win32_handle) => {
            log::info!("Found Win32 window handle: {:?}", win32_handle);
            get_win32_surface(instance, entry, win32_handle)
        }
        handle => {
            log::error!("{:?} is not supported yet!", handle);
            None
        }
    }
}

#[cfg(target_os = "linux")]
fn get_xlib_surface(
    instance: &ash::Instance,
    entry: &ash::Entry,
    xlib_handle: &raw_window_handle::XlibHandle,
) -> Option<ash::vk::SurfaceKHR> {
    let xlib_surface_create_info = ash::vk::XlibSurfaceCreateInfoKHR::builder()
        .dpy(xlib_handle.display as *mut _)
        .window(xlib_handle.window);
    log::debug!("Xlib surface create info created!");
    unsafe {
        let surface_factory =
            ash::extensions::khr::XlibSurface::new(entry, instance);
        match surface_factory
            .create_xlib_surface(&xlib_surface_create_info, None)
        {
            Ok(surface) => {
                log::info!("Xlib surface created: {:?}", surface);
                Some(surface)
            }
            Err(error) => {
                log::error!("Xlib surface initialization failed: {}", error);
                None
            }
        }
    }
}

#[cfg(target_os = "windows")]
fn get_win32_surface(
    instance: &ash::Instance,
    entry: &ash::Entry,
    win32_handle: raw_window_handle::windows::WindowsHandle,
) -> Option<ash::vk::SurfaceKHR> {
    let win32_surface_create_info =
        ash::vk::Win32SurfaceCreateInfoKHR::builder()
            .hinstance(win32_handle.hinstance)
            .hwnd(win32_handle.hwnd);
    unsafe {
        match ash::extensions::khr::Win32Surface::new(entry, instance)
            .create_win32_surface(&win32_surface_create_info, None)
        {
            Ok(surface) => Some(surface),
            Err(error) => {
                log::error!("Win32 surface initialization failed: {}", error);
                None
            }
        }
    }
}
