use ash::vk;
use crate::vulkan::QueueFamilyIndices;
use super::shaders::VulkanShaders;
use super::pipeline::{
    create_graphics_pipeline_defaults,
    create_viewport,
    create_scissor,
    create_color_blend_attachment,
};
use super::vulkan::{Renderer, Vulkan};

pub struct SwapchainSupportDetails {
    pub capabilities: vk::SurfaceCapabilitiesKHR,
    pub formats: std::vec::Vec<vk::SurfaceFormatKHR>,
    pub presentation_modes: std::vec::Vec<vk::PresentModeKHR>,
}

impl SwapchainSupportDetails {
    pub fn is_swap_chain_supported(&self) -> bool {
        let formats_valid = self.formats.len() > 0;
        let presentation_modes_valid = self.presentation_modes.len() > 0;
        formats_valid && presentation_modes_valid
    }

    pub fn choose_surface_format(&self) -> vk::SurfaceFormatKHR {
        for available_format in self.formats.iter() {
            match (available_format.format, available_format.color_space) {
                (
                    vk::Format::B8G8R8A8_SRGB,
                    vk::ColorSpaceKHR::SRGB_NONLINEAR,
                ) => {
                    log::info!(
                        "Found perfect surface format: {:?}",
                        available_format
                    );
                    return available_format.clone();
                }
                (skipping_format, skipping_colorspace) => {
                    log::trace!(
                        "Skipping format: {:?}, color space: {:?}",
                        skipping_format,
                        skipping_colorspace
                    );
                }
            };
        }
        let result = self
            .formats
            .first()
            .expect("Formats are not empty!")
            .clone();
        log::warn!(
            "Cannot find perfect surface format! Selecting the first one: {:?}",
            result
        );
        result
    }

    pub fn choose_surface_presentation_mode(&self) -> vk::PresentModeKHR {
        for presentation_mode in self.presentation_modes.iter() {
            match presentation_mode {
                &vk::PresentModeKHR::MAILBOX => {
                    log::info!(
                        "Found perfect presentation mode: {:?}",
                        presentation_mode
                    );
                    return presentation_mode.clone();
                }
                _ => (),
            };
        }
        log::info!(
            "Choose default presentation mode: {:?}",
            vk::PresentModeKHR::FIFO
        );
        return vk::PresentModeKHR::FIFO;
    }

    pub fn choose_swap_extent(&self) -> vk::Extent2D {
        match self.capabilities.current_extent.width {
            u32::MAX => unimplemented!("Calculate valid image extent"),
            _ => {
                /*
                log::info!(
                    "Choosing current extent: {:?}",
                    self.capabilities.current_extent
                );
                */
                self.capabilities.current_extent
            }
        }
    }
}

pub fn query_swap_chain_details(
    surface_loader: &ash::extensions::khr::Surface,
    surface: vk::SurfaceKHR,
    device: vk::PhysicalDevice,
) -> Option<SwapchainSupportDetails> {
    let capabilities = unsafe {
        match surface_loader
            .get_physical_device_surface_capabilities(device, surface)
        {
            Ok(capabilities) => capabilities,
            Err(error) => {
                log::error!(
                    "Cannot get physica device surface capabilities! {}",
                    error
                );
                return None;
            }
        }
    };
    let formats = unsafe {
        match surface_loader
            .get_physical_device_surface_formats(device, surface)
        {
            Ok(formats) => formats,
            Err(error) => {
                log::error!(
                    "Cannot get physical device surface formats! {}",
                    error
                );
                return None;
            }
        }
    };
    let presentation_modes = unsafe {
        match surface_loader
            .get_physical_device_surface_present_modes(device, surface)
        {
            Ok(presentation_modes) => presentation_modes,
            Err(error) => {
                log::error!(
                    "Cannot get physical device surface presentation modes! {}",
                    error
                );
                return None;
            }
        }
    };
    Some(SwapchainSupportDetails {
        capabilities,
        formats,
        presentation_modes,
    })
}

pub fn create_swapchain(
    swap_chain_loader: ash::extensions::khr::Swapchain,
    surface: vk::SurfaceKHR,
    swap_chain_support_details: &SwapchainSupportDetails,
    queue_family_indices: &QueueFamilyIndices,
) -> Option<vk::SwapchainKHR> {
    let mut image_count =
        swap_chain_support_details.capabilities.min_image_count + 1;
    if swap_chain_support_details.capabilities.max_image_count > 0 {
        image_count = std::cmp::min(
            image_count,
            swap_chain_support_details.capabilities.max_image_count,
        );
    }

    let surface_format = swap_chain_support_details.choose_surface_format();
    let surface_presentation_mode =
        swap_chain_support_details.choose_surface_presentation_mode();
    let swap_extent = swap_chain_support_details.choose_swap_extent();

    let mut swap_chain_create_info_builder =
        vk::SwapchainCreateInfoKHR::builder()
            .surface(surface)
            .min_image_count(image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(swap_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT);

    let indices = [
        queue_family_indices.graphics_family.unwrap(),
        queue_family_indices.presentation_family.unwrap(),
    ];
    if indices[0] != indices[1] {
        log::trace!("The graphics queue family is different from the presentation queue");
        swap_chain_create_info_builder = swap_chain_create_info_builder
            .image_sharing_mode(vk::SharingMode::CONCURRENT)
            .queue_family_indices(&indices);
    } else {
        log::trace!(
            "The graphics queue family is the same as the presentation queue"
        );
        swap_chain_create_info_builder = swap_chain_create_info_builder
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE);
    }
    let swap_chain_create_info = swap_chain_create_info_builder
        .pre_transform(
            swap_chain_support_details.capabilities.current_transform,
        )
        .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
        .present_mode(surface_presentation_mode)
        .clipped(true)
        .build();
    unsafe {
        swap_chain_loader
            .create_swapchain(&swap_chain_create_info, None)
            .ok()
    }
}

pub fn create_image_views(
    swap_chain_loader: ash::extensions::khr::Swapchain,
    swap_chain: vk::SwapchainKHR,
    swap_chain_support_details: &SwapchainSupportDetails,
    device: &ash::Device,
) -> Option<Vec<vk::ImageView>> {
    let swap_chain_images =
        unsafe { swap_chain_loader.get_swapchain_images(swap_chain).ok()? };
    Some(
        swap_chain_images
            .into_iter()
            .map(|image| {
                let image_view_create_info =
                    vk::ImageViewCreateInfo::builder()
                        .view_type(vk::ImageViewType::TYPE_2D)
                        .image(image)
                        .format(
                            swap_chain_support_details
                                .choose_surface_format()
                                .format,
                        )
                        .components(
                            vk::ComponentMapping::builder()
                                .r(vk::ComponentSwizzle::R)
                                .g(vk::ComponentSwizzle::G)
                                .b(vk::ComponentSwizzle::B)
                                .a(vk::ComponentSwizzle::A)
                                .build(),
                        )
                        .subresource_range(
                            vk::ImageSubresourceRange::builder()
                                .aspect_mask(vk::ImageAspectFlags::COLOR)
                                .base_mip_level(0)
                                .level_count(1)
                                .base_array_layer(0)
                                .layer_count(1)
                                .build(),
                        )
                        .build();
                unsafe {
                    device
                        .create_image_view(&image_view_create_info, None)
                        .unwrap()
                }
            })
            .collect(),
    )
}

pub fn create_render_pass(
    support_details: &SwapchainSupportDetails,
    logical_device: &ash::Device,
) -> Result<vk::RenderPass, vk::Result> {
    let color_attachment = vk::AttachmentDescription::builder()
        .format(support_details.choose_surface_format().format)
        .samples(vk::SampleCountFlags::TYPE_1)
        .load_op(vk::AttachmentLoadOp::CLEAR)
        .store_op(vk::AttachmentStoreOp::STORE)
        .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
        .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
        .initial_layout(vk::ImageLayout::UNDEFINED)
        .final_layout(vk::ImageLayout::PRESENT_SRC_KHR)
        .build();
    let color_attachment_ref = vk::AttachmentReference::builder()
        .attachment(0)
        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
        .build();
    let references = vec![color_attachment_ref];
    let subpass = vk::SubpassDescription::builder()
        .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
        .color_attachments(&references)
        .build();

    let attachments = vec![color_attachment];
    let subpasses = vec![subpass];
    let subpass_dependency = vk::SubpassDependency::builder()
        .src_subpass(vk::SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ) // ERROR MAY BE HERE
        .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
        .build();
    let dependencies = [subpass_dependency];

    let render_pass_info = vk::RenderPassCreateInfo::builder()
        .attachments(&attachments)
        .subpasses(&subpasses)
        .dependencies(&dependencies)
        .build();

    unsafe { 
        logical_device.create_render_pass(&render_pass_info, None)
    }
}

pub fn create_graphics_pipeline(
    logical_device: &ash::Device,
    shader_stages: &[vk::PipelineShaderStageCreateInfo],
    vertex_input_state: &vk::PipelineVertexInputStateCreateInfo,
    input_assembly_state: &vk::PipelineInputAssemblyStateCreateInfo,
    viewport_state: &vk::PipelineViewportStateCreateInfo,
    rasterization_state: &vk::PipelineRasterizationStateCreateInfo,
    multisample_state: &vk::PipelineMultisampleStateCreateInfo,
    color_blend_state: &vk::PipelineColorBlendStateCreateInfo,
    pipeline_layout: vk::PipelineLayout,
    render_pass: vk::RenderPass,
) -> Result<Vec<vk::Pipeline>, (Vec<vk::Pipeline>, vk::Result)> {
    let pipeline_info = vk::GraphicsPipelineCreateInfo::builder()
        .stages(shader_stages)
        .vertex_input_state(&vertex_input_state)
        .input_assembly_state(&input_assembly_state)
        .viewport_state(&viewport_state)
        .rasterization_state(&rasterization_state)
        .multisample_state(&multisample_state)
        .color_blend_state(&color_blend_state)
        .layout(pipeline_layout)
        .render_pass(render_pass)
        .subpass(0)
        .build();

    let pipelines = vec![pipeline_info];
    let pipeline_cache_info = vk::PipelineCacheCreateInfo::builder()
        .build();
    let pipeline_cache = unsafe {
        logical_device.create_pipeline_cache(
            &pipeline_cache_info,
            None)
            .expect("Failed to create Pipeline Cache")
    };

    unsafe {
        logical_device.create_graphics_pipelines(
            pipeline_cache,
            &pipelines,
            None
        )
    }
}

pub fn create_framebuffers(
    logical_device: &ash::Device,
    swapchain_extent: &vk::Extent2D,
    image_views: &[vk::ImageView],
    render_pass: vk::RenderPass,
) -> Result<Vec<vk::Framebuffer>, vk::Result> {
    let mut framebuffers: Vec<vk::Framebuffer> = Vec::new();
    for image_view in image_views.iter() {
        let attachments = [*image_view];
        let framebuffer_create_info = vk::FramebufferCreateInfo::builder()
            .render_pass(render_pass)
            .attachments(&attachments)
            .width(swapchain_extent.width)
            .height(swapchain_extent.height)
            .layers(1)
            .build();
        framebuffers.push(unsafe {
            logical_device.create_framebuffer(&framebuffer_create_info, None)?
        });
    };
    Ok(framebuffers)
}

pub fn create_command_buffers(
    logical_device: &ash::Device,
    command_pool: vk::CommandPool,
    image_views_count: u32,
) -> Result<Vec<vk::CommandBuffer>, vk::Result> {
    let command_buffer_allocate_info = vk::CommandBufferAllocateInfo::builder()
        .command_pool(command_pool)
        .level(vk::CommandBufferLevel::PRIMARY)
        .command_buffer_count(image_views_count)
        .build();
    unsafe {
        logical_device.allocate_command_buffers(&command_buffer_allocate_info)
    }
}

pub fn draw(
    logical_device: &ash::Device,
    framebuffers: &[vk::Framebuffer],
    command_pool: vk::CommandPool,
    command_buffers: &[vk::CommandBuffer],
    render_pass: vk::RenderPass,
    graphics_pipeline: vk::Pipeline,
    buffers: &[vk::Buffer],
    swapchain_extent: vk::Extent2D,
) -> Result<(), vk::Result> {
    unsafe {
        logical_device.reset_command_pool(
            command_pool,
            vk::CommandPoolResetFlags::RELEASE_RESOURCES)?;
    };
    for (command_buffer, framebuffer) in command_buffers.iter().zip(framebuffers.iter()) {
        let command_buffer_begin_info = vk::CommandBufferBeginInfo::builder()
            .build();
        let clear_color = vk::ClearValue {
            color: vk::ClearColorValue { 
                float32: [0.01f32, 0.01f32, 0.02f32, 1f32] 
            }
        };
        let clear_values = [clear_color];
        let render_pass_begin_info = vk::RenderPassBeginInfo::builder()
            .render_pass(render_pass)
            .framebuffer(*framebuffer)
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: swapchain_extent
            })
            .clear_values(&clear_values)
            .build();
        unsafe {
            logical_device.begin_command_buffer(*command_buffer, &command_buffer_begin_info)?;
            logical_device.cmd_begin_render_pass(*command_buffer, &render_pass_begin_info, vk::SubpassContents::INLINE);

            logical_device.cmd_bind_pipeline(*command_buffer, vk::PipelineBindPoint::GRAPHICS, graphics_pipeline);
            logical_device.cmd_bind_vertex_buffers(*command_buffer, 0, buffers, &[0]);

            //logical_device.cmd_draw(*command_buffer, vertex_buffer.len as u32, 1, 0, 0);
            logical_device.cmd_draw(*command_buffer, 6, 1, 0, 0);

            logical_device.cmd_end_render_pass(*command_buffer);
            logical_device.end_command_buffer(*command_buffer)?
        };
    }
    Ok(())
}

pub fn reset_swapchain(
    vulkan: &Vulkan,
    renderer: &mut Renderer,
    shaders: &VulkanShaders,
) -> Option<()> {
    unsafe {
        vulkan.logical_device.device_wait_idle()
            .expect("Failed to wait device on swapchain reset!");
    };

    let surface_loader =
        ash::extensions::khr::Surface::new(&vulkan.entry, &vulkan.instance);
    let support_details = query_swap_chain_details(
        &surface_loader,
        vulkan.surface.clone(),
        vulkan.physical_device,
    )?;
    renderer.swapchain = create_swapchain(
        renderer.swapchain_loader.clone(),
        vulkan.surface,
        &support_details,
        &vulkan.queue_family_indices)?;
    renderer.image_views = create_image_views(
        renderer.swapchain_loader.clone(),
        renderer.swapchain,
        &support_details,
        &vulkan.logical_device)?;
    renderer.render_pass = create_render_pass(&support_details, &vulkan.logical_device).ok()?;
    let extent_2d = support_details.choose_swap_extent();
    let pipeline_defaults = create_graphics_pipeline_defaults(
        vec![create_viewport(&extent_2d)],
        vec![create_scissor(&extent_2d)],
        vec![create_color_blend_attachment()]
    )?;
    renderer.graphics_pipeline = create_graphics_pipeline(
        &vulkan.logical_device,
        &shaders.shaders_stages,
        &shaders.pipeline_vertex_input_state,
        &pipeline_defaults.input_assembly,
        &pipeline_defaults.viewport_state,
        &pipeline_defaults.rasterizer,
        &pipeline_defaults.multisampling,
        &pipeline_defaults.color_blending,
        renderer.pipeline_layout,
        renderer.render_pass
    ).ok()?;
    renderer.framebuffers = create_framebuffers(
        &vulkan.logical_device,
        &extent_2d,
        &renderer.image_views,
        renderer.render_pass
    ).ok()?;
    renderer.command_buffers = create_command_buffers(&vulkan.logical_device, renderer.command_pool, renderer.image_views.len() as u32).ok()?;

    Some(())
}
