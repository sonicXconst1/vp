use super::shaders::VulkanShaders;
use super::surface;
use super::swapchain::{
    SwapchainSupportDetails,
    query_swap_chain_details,
    create_swapchain,
    create_image_views,
    create_render_pass,
    create_graphics_pipeline,
    create_framebuffers,
    create_command_buffers,
};
use super::pipeline::{
    create_viewport,
    create_scissor,
    create_color_blend_attachment,
    create_graphics_pipeline_defaults
};
use super::utils;
use ash::vk;
use ash::extensions::khr;
use log;

#[derive(Copy, Clone)]
pub struct QueueFamilyIndices {
    pub graphics_family: Option<u32>,
    pub presentation_family: Option<u32>,
}

pub struct Vulkan {
    pub entry: ash::Entry,
    pub instance: ash::Instance,

    pub physical_device: vk::PhysicalDevice,
    // Do I need to request them all the time or during init only?
    pub memory_properties: vk::PhysicalDeviceMemoryProperties,

    pub queue_family_indices: QueueFamilyIndices,
    pub logical_device: ash::Device,
    pub device_queue: vk::Queue,

    pub surface: vk::SurfaceKHR,
    pub surface_loader: khr::Surface,
    pub swapchain_support_details: SwapchainSupportDetails
}

pub struct Renderer {
    pub swapchain_loader: ash::extensions::khr::Swapchain,
    pub swapchain: vk::SwapchainKHR,

    pub image_views: Vec<vk::ImageView>,

    pub render_pass: vk::RenderPass,

    pub pipeline_layout: vk::PipelineLayout,
    pub graphics_pipeline: Vec<vk::Pipeline>,

    pub framebuffers: Vec<vk::Framebuffer>,

    pub command_pool: vk::CommandPool,
    pub command_buffers: Vec<vk::CommandBuffer>,

    pub image_available_semaphore: vk::Semaphore,
    pub render_finished_semaphore: vk::Semaphore
}

impl Vulkan {
    pub fn new(
        window_handle: &dyn raw_window_handle::HasRawWindowHandle,
        validation_layers: &[&str]
    ) -> Option<Self> {
        let entry = ash::Entry::linked();
        let application_info = create_application_info(&entry)
            .expect("Application info initialized!");

        let layers = utils::strs_as_cstrings(&validation_layers);
        let extensions = utils::strings_as_cstrings(
            &enumerate_required_extensions(window_handle),
        );

        if !are_layers_valid(&entry, &layers) {
            return None;
        }

        let enabled_layers = utils::cstrings_as_pointers(&layers);
        let enabled_extensions = utils::cstrings_as_pointers(&extensions);

        let instance_create_info = create_instance_create_info(
            &application_info,
            &enabled_layers,
            &enabled_extensions,
        );

        let instance = create_vulkan_instance(&entry, &instance_create_info)
            .expect("Instance initialized!");

        let surface = surface::create_surface(&instance, &entry, window_handle)
            .expect("Surface can be initialized!");

        let surface_loader = khr::Surface::new(&entry, &instance);
        let (physical_device, queue_family_indices) =
            initialize_physical_device(&instance, &surface_loader, &surface)
                .expect("Physical device can be initialized.");

        let device_extensions = [std::ffi::CString::from(
            ash::extensions::khr::Swapchain::name(),
        )];
        let device_extensions_raw =
            utils::cstrings_as_pointers(&device_extensions);

        let logical_device: ash::Device = initialize_logical_device(
            &instance,
            physical_device,
            &enabled_layers,
            &queue_family_indices,
            &device_extensions_raw,
        )?;

        let device_queue: vk::Queue =
            initialize_device_queue(&logical_device, &queue_family_indices)
                .expect("Device queue can be initialized!");

        if !is_swap_chain_available(
            &instance,
            physical_device,
            &device_extensions,
        ) {
            log::error!("Swap chain is not available!");
            return None;
        }

        let swapchain_support_details = query_swap_chain_details(
            &surface_loader,
            surface.clone(),
            physical_device,
        )?;

        if !swapchain_support_details.is_swap_chain_supported() {
            log::error!("Swap chain is not supported!");
            return None;
        }

        let memory_properties = unsafe {
            instance.get_physical_device_memory_properties(physical_device)
        };

        Some(Vulkan {
            entry,
            instance,
            physical_device,
            memory_properties,
            queue_family_indices,
            logical_device,
            device_queue,
            surface,
            surface_loader,
            swapchain_support_details
        })
    }
}

impl Renderer {
    pub fn new(vulkan: &Vulkan, shaders: &VulkanShaders) -> Option<Self> {
        let Vulkan {
            entry: _entry,
            instance,
            physical_device,
            memory_properties: _,
            logical_device,
            surface,
            surface_loader,
            device_queue: _device_queue,
            queue_family_indices,
            swapchain_support_details,
        } = vulkan;

        let pipeline_layout_info = vk::PipelineLayoutCreateInfo::builder()
            .build();

        log::info!("Creating Pipeline Layout...");

        let pipeline_layout = unsafe {
            logical_device.create_pipeline_layout(&pipeline_layout_info, None)
                .expect("Failed to craete pipeline layout")
        };

        let support_details = query_swap_chain_details(&surface_loader, *surface, *physical_device)?;
        let swapchain_loader = khr::Swapchain::new(&instance, &logical_device);
        let swapchain = create_swapchain(
            swapchain_loader.clone(),
            *surface,
            &support_details,
            &queue_family_indices,
        )?;
        let image_views = create_image_views(
            swapchain_loader.clone(),
            swapchain,
            &support_details,
            &logical_device)?;
        let render_pass = create_render_pass(&support_details, &logical_device).ok()?;

        let extent_2d = swapchain_support_details.choose_swap_extent();
        let pipeline_defaults = create_graphics_pipeline_defaults(
            vec![create_viewport(&extent_2d)],
            vec![create_scissor(&extent_2d)],
            vec![create_color_blend_attachment()]
        )?;
        log::info!("Render Pass created!");
        let graphics_pipeline = create_graphics_pipeline(
            &logical_device,
            &shaders.shaders_stages,
            &shaders.pipeline_vertex_input_state,
            &pipeline_defaults.input_assembly,
            &pipeline_defaults.viewport_state,
            &pipeline_defaults.rasterizer,
            &pipeline_defaults.multisampling,
            &pipeline_defaults.color_blending,
            pipeline_layout,
            render_pass
        ).ok()?;
        log::info!("Graphics Pipeline created!");
        
        let swapchain_extent = support_details.choose_swap_extent();
        let framebuffers = create_framebuffers(
            &logical_device,
            &swapchain_extent,
            &image_views,
            render_pass
        ).ok()?;

        let command_pool_info = vk::CommandPoolCreateInfo::builder()
            .queue_family_index(queue_family_indices.graphics_family.unwrap())
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .build();

        let command_pool = unsafe {
            logical_device.create_command_pool(&command_pool_info, None)
                .expect("Failed to create command poll!")
        };

        let command_buffers = create_command_buffers(
            &logical_device,
            command_pool,
            image_views.len() as u32
        ).ok()?;

        let semaphore_info = vk::SemaphoreCreateInfo::builder()
            .build();

        let image_available_semaphore = unsafe {
            logical_device.create_semaphore(&semaphore_info, None)
                .expect("Failed to create image available semaphore!")
        };
        let render_finished_semaphore = unsafe {
            logical_device.create_semaphore(&semaphore_info, None)
                .expect("Failed to create render finished semaphore!")
        };

        Some(Renderer {
            swapchain_loader,
            swapchain,
            image_views,
            render_pass,
            pipeline_layout,
            graphics_pipeline,
            framebuffers,
            command_pool,
            command_buffers,
            image_available_semaphore,
            render_finished_semaphore,
        })
    }
}

fn create_application_info(
    entry: &ash::Entry,
) -> Option<vk::ApplicationInfo> {
    let application_version =
        match entry.try_enumerate_instance_version().ok()? {
            Some(version) => vk::make_api_version(
                vk::api_version_variant(version),
                vk::api_version_major(version),
                vk::api_version_minor(version),
                vk::api_version_patch(version),
            ),
            None => vk::make_api_version(0, 1, 0, 0),
        };
    log::info!("Application version: {}", application_version);
    log::trace!("Creating the Application Info...");
    let result = Some(
        vk::ApplicationInfo::builder()
            .application_name(
                &std::ffi::CString::new("Vulkan Playground").unwrap(),
            )
            .engine_name(&std::ffi::CString::new("VP_ENGINE").unwrap())
            .application_version(application_version)
            .build(),
    );
    log::trace!("The Application Info is created!");
    return result;
}

fn create_instance_create_info(
    application_info: &vk::ApplicationInfo,
    layers: &[*const i8],
    extensions: &[*const i8],
) -> vk::InstanceCreateInfo {
    log::trace!("Creating the Instance Create Info...");
    let instance_create_info = vk::InstanceCreateInfo::builder()
        .application_info(application_info)
        .enabled_layer_names(layers)
        .enabled_extension_names(extensions)
        .build();
    log::trace!("The Instance Create Info is created!");
    instance_create_info
}

fn create_vulkan_instance(
    entry: &ash::Entry,
    instance_create_info: &vk::InstanceCreateInfo,
) -> Option<ash::Instance> {
    log::trace!("Creating an Vulkan instance...");
    unsafe {
        match entry.create_instance(&instance_create_info, None) {
            Ok(instance) => {
                log::info!("Instance created...");
                Some(instance)
            }
            Err(error) => {
                log::error!("Error while creating an instance: {}", error);
                None
            }
        }
    }
}

fn are_layers_valid(
    entry: &ash::Entry,
    layers: &[std::ffi::CString],
) -> bool {
    let available_layers: Vec<_> =
        match entry.enumerate_instance_layer_properties() {
            Ok(layers) => layers
                .iter()
                .map(|layer| unsafe {
                    std::ffi::CString::from(std::ffi::CStr::from_ptr(
                        layer.layer_name.as_ptr(),
                    ))
                })
                .collect(),
            Err(_) => {
                log::error!("Cannot find instance layer properties!");
                return false;
            }
        };
    log::trace!("Available layers: {available_layers:?}");
    let result = layers.iter().all(move |layer| {
        match available_layers
            .iter()
            .find(|available_layer| available_layer == &layer)
        {
            Some(_value) => {
                log::info!("{:?} layer is available!", layer);
                true
            }
            None => {
                log::error!("{:?} layer is not available!", layer);
                false
            }
        }
    });
    if result {
        log::info!("The layers for the Instance Create Info are valid!");
    } else {
        log::error!("The layers are invalid for the Instance Create Info");
    }
    return result;
}

fn initialize_device_queue(
    device: &ash::Device,
    device_queue_family_indices: &QueueFamilyIndices,
) -> Option<vk::Queue> {
    let graphics_family_index =
        match device_queue_family_indices.graphics_family {
            Some(index) => index,
            None => {
                log::error!("Cannot find graphics queue family index:");
                return None;
            }
        };
    log::trace!("Graphics family index: {graphics_family_index}");
    let result = unsafe { device.get_device_queue(graphics_family_index, 0) };
    log::trace!("Device Queue: {result:?}");
    Some(result)
}

fn initialize_logical_device(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    layer_names: &[*const i8],
    device_queue_family_indices: &QueueFamilyIndices,
    device_extension_names: &[*const i8],
) -> Option<ash::Device> {
    let graphics_family_index =
        match device_queue_family_indices.graphics_family {
            Some(index) => index,
            None => {
                log::error!("Cannot find graphics queue family index.");
                return None;
            }
        };
    let presentation_family_index =
        match device_queue_family_indices.presentation_family {
            Some(index) => index,
            None => {
                log::error!("Cannot find presentation queue family index.");
                return None;
            }
        };
    let mut device_queue_create_infos =
        Vec::<vk::DeviceQueueCreateInfo>::new();
    device_queue_create_infos.push(
        vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(graphics_family_index)
            .queue_priorities(&[1.0])
            .build(),
    );
    if graphics_family_index != presentation_family_index {
        vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(graphics_family_index)
            .queue_priorities(&[1.0])
            .build();
    }
    log::trace!("Device queue create info created!");
    let physical_device_features =
        unsafe { instance.get_physical_device_features(device) };
    let device_create_info = vk::DeviceCreateInfo::builder()
        .queue_create_infos(&device_queue_create_infos)
        .enabled_features(&physical_device_features)
        .enabled_layer_names(layer_names)
        .enabled_extension_names(device_extension_names)
        .build();
    unsafe {
        match instance.create_device(device, &device_create_info, None) {
            Ok(device) => {
                log::info!("Logical device created");
                Some(device)
            }
            Err(error) => {
                log::error!("Failed to create logical device: {}", error);
                None
            }
        }
    }
}

fn initialize_physical_device(
    instance: &ash::Instance,
    surface_loader: &ash::extensions::khr::Surface,
    surface: &vk::SurfaceKHR,
) -> Option<(vk::PhysicalDevice, QueueFamilyIndices)> {
    let physical_devices = peek_physical_devices(instance)
        .expect("We are able to peek physical devices.");
    for device in physical_devices {
        let device_queue_family_indices = find_queue_families(
            &instance,
            device,
            &surface_loader,
            surface.clone(),
        );
        log::debug!(
            "Device queue family indices: GRAPHICS_{:?}, PRESENTATION_{:?}",
            device_queue_family_indices.graphics_family,
            device_queue_family_indices.presentation_family
        );
        if is_device_suitable(instance, device, &device_queue_family_indices) {
            return Some((device, device_queue_family_indices));
        }
    }
    None
}

fn peek_physical_devices(
    instance: &ash::Instance,
) -> Option<Vec<vk::PhysicalDevice>> {
    unsafe {
        match instance.enumerate_physical_devices() {
            Ok(devices) => {
                log::info!("{} physical devices are available", devices.len());
                if devices.len() == 0 {
                    log::error!("Failed to find GPU with Vulkan support");
                    None
                } else {
                    Some(devices)
                }
            }
            Err(error) => {
                log::error!("Unable to peek physical devices: {}", error);
                None
            }
        }
    }
}

fn is_device_suitable(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    device_queue_family_indices: &QueueFamilyIndices,
) -> bool {
    let device_properties =
        unsafe { instance.get_physical_device_properties(device) };
    match device_properties.device_type {
        vk::PhysicalDeviceType::OTHER => {
            return false;
        }
        device_type => {
            log::info!("Physical device of type {:?} found!", device_type)
        }
    };
    let device_features =
        unsafe { instance.get_physical_device_features(device) };
    if device_features.geometry_shader > 0 {
        log::info!("Physical device support geometry shader!");
    } else {
        return false;
    }
    if let Some(_graphics_family) = device_queue_family_indices.graphics_family
    {
        log::info!(
            "The graphics queue family index found for the Physical device"
        );
        true
    } else {
        log::info!(
            "The graphics queue family index not found for the Physical device"
        );
        false
    }
}

fn is_swap_chain_available(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    extension_names: &[std::ffi::CString],
) -> bool {
    let device_extension_properties = unsafe {
        instance
            .enumerate_device_extension_properties(device)
            .expect("Cannot get device extension properties from instance!")
    };
    log::trace!("Device properties: {device_extension_properties:?}");
    let available_device_extensions: Vec<std::ffi::CString> = unsafe {
        device_extension_properties
            .iter()
            .map(|property| {
                std::ffi::CString::from(std::ffi::CStr::from_ptr(property.extension_name.as_ptr()))
            })
            .collect()
    };
    log::trace!("Available extensions: {available_device_extensions:?}");
    extension_names.iter().all(|required_extension| {
        available_device_extensions
            .iter()
            .any(|available_extension| {
                available_extension == required_extension
            })
    })
}


fn find_queue_families(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    surface_loader: &ash::extensions::khr::Surface,
    surface: vk::SurfaceKHR,
) -> QueueFamilyIndices {
    let device_queue_family_properties =
        unsafe { instance.get_physical_device_queue_family_properties(device) };
    let mut result: QueueFamilyIndices = QueueFamilyIndices {
        graphics_family: None,
        presentation_family: None,
    };
    log::trace!(
        "Found {} queue families!",
        device_queue_family_properties.len()
    );
    let mut index = 0;
    for device_queue_family_property in device_queue_family_properties {
        if device_queue_family_property
            .queue_flags
            .contains(vk::QueueFlags::GRAPHICS)
        {
            log::trace!(
                "Graphics family found: {:?}",
                device_queue_family_property
            );
            result.graphics_family = Some(index)
        }
        unsafe {
            match surface_loader
                .get_physical_device_surface_support(device, index, surface)
            {
                Ok(support) => {
                    if support {
                        log::trace!(
                            "Present family found: {:?}",
                            device_queue_family_property
                        );
                        result.presentation_family = Some(index);
                    }
                }
                Err(error) => {
                    log::error!(
                        "Error in finding present family index: {}",
                        error
                    );
                }
            };
        }
        index += 1;
    }
    result
}

fn enumerate_required_extensions(
    window_handle: &dyn raw_window_handle::HasRawWindowHandle,
) -> std::vec::Vec<String> {
    match window_handle.raw_window_handle() {
        #[cfg(target_os = "linux")]
        raw_window_handle::RawWindowHandle::Xlib(_x_lib_handle) => vec![
            String::from(
                ash::extensions::khr::Surface::name().to_str().unwrap(),
            ),
            String::from(
                ash::extensions::khr::XlibSurface::name().to_str().unwrap(),
            ),
        ],
        #[cfg(target_os = "windows")]
        raw_window_handle::RawWindowHandle::Windows(_windows_handle) => vec![
            String::from(
                ash::extensions::khr::Surface::name().to_str().unwrap(),
            ),
            String::from(
                ash::extensions::khr::Win32Surface::name().to_str().unwrap(),
            ),
        ],
        _ => vec![],
    }
}
