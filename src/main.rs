#![feature(vec_into_raw_parts)]

pub mod domain;
pub mod logger;
pub mod surface;
pub mod utils;
pub mod vulkan;
pub mod shaders;
pub mod shader_compiler;
pub mod buffer;
pub mod swapchain;
pub mod pipeline;

use std::ffi::CString;

use log;
use winit::event_loop::EventLoop;
use winit::window::{WindowBuilder, Window};
use ash::vk;

use buffer::VertexBuffer;


const VALIDATION_LAYERS: [&str; 2] = [
    "VK_LAYER_KHRONOS_validation",
    "VK_LAYER_LUNARG_api_dump"
];

fn main() {
    const LOGGER: logger::Logger = logger::Logger{};
    log::set_logger(&LOGGER).expect("Failed to set the logger!");
    log::trace!("Logger is enabled!");
    LOGGER.init();

    let event_loop = winit::event_loop::EventLoop::new();
    let window = create_window(&event_loop);
    run(event_loop, window)
}

fn create_window(event_loop: &EventLoop<()>) -> Window {
    WindowBuilder::new().build(&event_loop)
        .expect("Failed to create a window!")
}

fn run(event_loop: EventLoop<()>, window: Window) -> ! {
    let vulkan = vulkan::Vulkan::new(&window, &VALIDATION_LAYERS)
        .expect("Failed to create vulkan!"); 

    let shader_compiler = shader_compiler::ShaderCompiler::new(
        std::path::PathBuf::from("glslc"));
    let (vertex_shader_code, fragment_shader_code) = shaders::compile_default_shaders(&shader_compiler)
        .expect("Failed to compile default shaders!");

    let vertex_shader_module = shaders::create_shader_module(
        &vulkan.logical_device,
        vertex_shader_code)
        .expect("Failed to create vertex shader module!");
    let fragment_shader_module = shaders::create_shader_module(
        &vulkan.logical_device,
        fragment_shader_code)
        .expect("Failed to create fragment shader module!");
    let shaders = shaders::create_vulkan_shaders(
        vertex_shader_module,
        fragment_shader_module,
        CString::new("main").unwrap());

    let mut renderer = vulkan::Renderer::new(&vulkan, &shaders)
        .expect("Failed to create renderer");
    let vertex_buffer = unsafe { 
        VertexBuffer::new(
            vulkan.memory_properties,
            &vulkan.logical_device,
            vec![
                domain::Vertex {
                    position: domain::Vec2 { x: -0.5, y: -0.5 },
                    color: domain::Color { r: 0.0, g: 0.0, b: 1.0, a: 1.0}
                },
                domain::Vertex {
                    position: domain::Vec2 { x: 0.5, y: -0.5 },
                    color: domain::Color { r: 1.0, g: 0.0, b: 0.0, a: 1.0}
                },
                domain::Vertex {
                    position: domain::Vec2 { x: 0.5, y: 0.5 },
                    color: domain::Color { r: 0.0, g: 1.0, b: 0.0, a: 1.0}
                },

                domain::Vertex {
                    position: domain::Vec2 { x: 0.5, y: 0.5 },
                    color: domain::Color { r: 0.0, g: 1.0, b: 0.0, a: 1.0}
                },
                domain::Vertex {
                    position: domain::Vec2 { x: -0.5, y: 0.5 },
                    color: domain::Color { r: 1.0, g: 1.0, b: 1.0, a: 1.0}
                },
                domain::Vertex {
                    position: domain::Vec2 { x: -0.5, y: -0.5 },
                    color: domain::Color { r: 0.0, g: 0.0, b: 1.0, a: 1.0}
                },
            ]
        ).expect("Buffer created")
    };

    event_loop.run(move |event, _, control_flow| {
        *control_flow = winit::event_loop::ControlFlow::Wait;
        match event {
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::CloseRequested,
                window_id,
            } if window_id == window.id() => {
                unsafe {
                    vulkan.logical_device.device_wait_idle()
                        .expect("Failed to wait device idle!");
                };
                *control_flow = winit::event_loop::ControlFlow::Exit
            }
            _ => {
                let (image_index, _suboptimal) = unsafe {
                    match renderer.swapchain_loader.acquire_next_image(
                        renderer.swapchain,
                        u64::MAX,
                        renderer.image_available_semaphore,
                        vk::Fence::null()

                    ) {
                        Ok(result) => result,
                        Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                            println!("RESET SWAPCHAIN");
                            swapchain::reset_swapchain(&vulkan, &mut renderer, &shaders);
                            return;
                        }
                        _ => panic!("Failed to acquire next image!")
                    }
                };
                let semaphores = [
                    renderer.image_available_semaphore
                ];
                let wait_stages = [
                    vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                ];
                let signal_semaphores = [
                    renderer.render_finished_semaphore
                ];

                let extent_2d = vulkan.swapchain_support_details.choose_swap_extent();
                let buffers = [vertex_buffer.buffer];
                swapchain::draw(
                    &vulkan.logical_device,
                    &renderer.framebuffers,
                    renderer.command_pool,
                    &renderer.command_buffers,
                    renderer.render_pass,
                    renderer.graphics_pipeline.get(0).unwrap().clone(),
                    &buffers,
                    extent_2d
                ).expect("Draw is executed seccessfully.");

                let command_buffer = renderer.command_buffers.get(image_index as usize)
                    .expect("Failed to get command buffer!");
                let buffers = [*command_buffer];
                let submit_info = vk::SubmitInfo::builder()
                    .command_buffers(&buffers)
                    .wait_semaphores(&semaphores)
                    .signal_semaphores(&signal_semaphores)
                    .wait_dst_stage_mask(&wait_stages)
                    .build();
                let submits = [submit_info];
                unsafe {
                    vulkan.logical_device.queue_submit(
                        vulkan.device_queue,
                        &submits,
                        vk::Fence::null())
                        .expect("Failed to queue submit!");
                };
                let swapchains = [renderer.swapchain];
                let image_indices = [image_index];
                let present_info = vk::PresentInfoKHR::builder()
                    .wait_semaphores(&signal_semaphores)
                    .swapchains(&swapchains)
                    .image_indices(&image_indices)
                    .build();

                unsafe {
                    match renderer.swapchain_loader.queue_present(vulkan.device_queue, &present_info) {
                        Ok(_) => (),
                        Err(vk::Result::ERROR_OUT_OF_DATE_KHR | vk::Result::SUBOPTIMAL_KHR) => {
                            swapchain::reset_swapchain(&vulkan, &mut renderer, &shaders);
                        },
                        Err(error) => panic!("Failed to present queue {error}"),
                    };
                    vulkan.logical_device.queue_wait_idle(vulkan.device_queue)
                        .expect("Failed to wait queue idle!");
                };
            },
        }
    });
}
