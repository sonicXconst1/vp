use ash::vk;


pub struct GraphicsPipelineDefaults {
    pub input_assembly: vk::PipelineInputAssemblyStateCreateInfo,
    pub viewport_state: vk::PipelineViewportStateCreateInfo,
    pub rasterizer: vk::PipelineRasterizationStateCreateInfo,
    pub multisampling: vk::PipelineMultisampleStateCreateInfo,
    pub color_blending: vk::PipelineColorBlendStateCreateInfo,
    pub viewports: Vec<vk::Viewport>,
    pub scissors: Vec<vk::Rect2D>,
    pub attachments: Vec<vk::PipelineColorBlendAttachmentState>
}

pub fn create_viewport(extent: &vk::Extent2D) -> vk::Viewport {
    vk::Viewport::builder()
        .x(0f32)
        .y(0f32)
        .width(extent.width as f32)
        .height(extent.height as f32)
        .min_depth(0f32)
        .max_depth(1f32)
        .build()
}

pub fn create_scissor(extent: &vk::Extent2D) -> vk::Rect2D {
    vk::Rect2D::builder()
        .offset(vk::Offset2D { x: 0, y: 0 })
        .extent(*extent)
        .build()
}

pub fn create_color_blend_attachment() -> vk::PipelineColorBlendAttachmentState {
    vk::PipelineColorBlendAttachmentState::builder()
        .color_write_mask(vk::ColorComponentFlags::R | vk::ColorComponentFlags::G | vk::ColorComponentFlags::B | vk::ColorComponentFlags::A)
        .blend_enable(false)
        .build()
}

pub fn create_graphics_pipeline_defaults(
    viewports: Vec<vk::Viewport>,
    scissors: Vec<vk::Rect2D>,
    attachments: Vec<vk::PipelineColorBlendAttachmentState>
) -> Option<GraphicsPipelineDefaults> {
    let input_assembly = vk::PipelineInputAssemblyStateCreateInfo::builder()
        .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
        .primitive_restart_enable(false)
        .build();

    let viewport_state = vk::PipelineViewportStateCreateInfo::builder()
        .viewports(&viewports)
        .scissors(&scissors)
        .build();

    let rasterizer = vk::PipelineRasterizationStateCreateInfo::builder()
        .depth_clamp_enable(false)
        .rasterizer_discard_enable(false)
        .polygon_mode(vk::PolygonMode::FILL)
        .cull_mode(vk::CullModeFlags::BACK)
        .front_face(vk::FrontFace::CLOCKWISE)
        .depth_bias_enable(false)
        .build();

    let multisampling = vk::PipelineMultisampleStateCreateInfo::builder()
        .sample_shading_enable(false)
        .rasterization_samples(vk::SampleCountFlags::TYPE_1)
        .build();

    let color_blending = vk::PipelineColorBlendStateCreateInfo::builder()
        .logic_op_enable(false)
        .attachments(&attachments)
        .build();

    Some(GraphicsPipelineDefaults {
        input_assembly,
        viewport_state,
        rasterizer,
        multisampling,
        color_blending,
        viewports,
        scissors,
        attachments
    })
}
