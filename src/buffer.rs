use ash::vk;

use crate::domain;

pub struct StagingBuffer {
    pub buffer: vk::Buffer,
    pub device_memory: vk::DeviceMemory,
    pub len: usize,
}

impl StaginBuffer {
    pub unsafe fn new(
        device: &ash::Device,
        memory_properties: vk::PhysicalDeviceMemoryProperties,
    ) -> Result<Self, vk::Result> {
    }
}

pub struct VertexBuffer {
    pub buffer: vk::Buffer,
    pub device_memory: vk::DeviceMemory,
    pub len: usize,
    data: *mut domain::Vertex,
}

impl VertexBuffer {
    pub unsafe fn new(
        memory_properties: vk::PhysicalDeviceMemoryProperties,
        device: &ash::Device,
        vertexes: Vec<domain::Vertex>,
    ) -> Result<VertexBuffer, vk::Result> {
        let buffer_create_info = vk::BufferCreateInfo::builder()
            .size((vertexes.len() * std::mem::size_of::<domain::Vertex>()) as vk::DeviceSize)
            .usage(vk::BufferUsageFlags::VERTEX_BUFFER)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .build();
        let buffer = device.create_buffer(&buffer_create_info, None)?;
        let memory_requirements = device.get_buffer_memory_requirements(buffer);
        // remove host coherent if you want to flush memory manually.
        let memory_flags = vk::MemoryPropertyFlags::DEVICE_LOCAL | vk::MemoryPropertyFlags::HOST_COHERENT;
        let memory_type_index = filter_memory_types(memory_properties, memory_requirements, memory_flags)
            .expect("Valid memory type has been found");
        let memory_allocation_info = vk::MemoryAllocateInfo::builder()
            .allocation_size(memory_requirements.size)
            .memory_type_index(memory_type_index)
            .build();
        let device_memory = device.allocate_memory(&memory_allocation_info, None)?;
        device.bind_buffer_memory(buffer, device_memory, 0)?;
        let data = device.map_memory(device_memory, 0, memory_allocation_info.allocation_size, vk::MemoryMapFlags::default())?;
        let data = data.cast::<domain::Vertex>();
        std::ptr::copy_nonoverlapping(vertexes.as_ptr(), data, vertexes.len());
        device.unmap_memory(device_memory);
        Ok(VertexBuffer { buffer, device_memory, data, len: vertexes.len() })
    }

    pub unsafe fn get(&self, index: usize) -> Option<domain::Vertex> {
        if index > self.len {
            None
        } else {
            Some(*self.data.add(index))
        }
    }
}


unsafe fn filter_memory_types(
    properties: vk::PhysicalDeviceMemoryProperties,
    requirements: vk::MemoryRequirements,
    flags: vk::MemoryPropertyFlags) -> Option<u32> {
    properties.memory_types[..properties.memory_type_count as usize]
        .iter()
        .enumerate()
        .find(|(index, memory_type)| {
            (1 << index) & requirements.memory_type_bits != 0
                && memory_type.property_flags & flags == flags
        })
        .map(|(index, _)| index as u32)
}

