use crate::domain::Vertex;

use std::ffi::CString;
use std::path::Path;

use ash::vk;

use super::shader_compiler::ShaderCompiler;

pub struct VulkanShaders {
    pub entry: std::ffi::CString,
    pub shaders_stages: Vec<vk::PipelineShaderStageCreateInfo>,
    pub pipeline_vertex_input_state: vk::PipelineVertexInputStateCreateInfo,
}

pub fn compile_default_shaders(compiler: &ShaderCompiler) -> Option<(Vec<u32>, Vec<u32>)>{
    if !compiler.compile(
        Path::new("shaders/shader.vert"),
        Path::new("shaders/vert.spv")
    ) {
        panic!("Failed to compile vertex shader!");
    }
    if !compiler.compile(
        Path::new("shaders/shader.frag"),
        Path::new("shaders/frag.spv")
    ) {
        panic!("Failed to compile fragment shader!");
    }
    Some((
        read_shader(Path::new("shaders/vert.spv")).ok()?,
        read_shader(Path::new("shaders/frag.spv")).ok()?
    ))
}

pub fn create_shader_module(
    device: &ash::Device,
    shader: Vec<u32>
) -> Result<vk::ShaderModule, vk::Result> {
    let module_create_info = vk::ShaderModuleCreateInfo::builder()
        .code(&shader);
    unsafe {
        device.create_shader_module(&module_create_info, None)
    }
}

fn read_shader(path: &std::path::Path) -> std::io::Result<Vec<u32>> {
    let mut file = std::fs::File::open(path)?;
    ash::util::read_spv(&mut file)
}

fn create_binding_description() -> vk::VertexInputBindingDescription {
    vk::VertexInputBindingDescription::builder()
        .binding(0)
        .stride(std::mem::size_of::<Vertex>() as u32)
        .input_rate(vk::VertexInputRate::VERTEX)
        .build()
}

fn create_attribute_description() -> [vk::VertexInputAttributeDescription; 2] {
    [
        vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(0)
            .format(vk::Format::R32G32_SFLOAT)
            .offset(memoffset::offset_of!(Vertex, position) as u32)
            .build(),
        vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(1)
            .format(vk::Format::R32G32B32A32_SFLOAT)
            .offset(memoffset::offset_of!(Vertex, color) as u32)
            .build()
    ]
}

pub fn create_vulkan_shaders(
    vertex: vk::ShaderModule,
    fragment: vk::ShaderModule,
    entry_point: CString) -> VulkanShaders {
    let vertex_shader_stage_create_info = vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::VERTEX)
        .module(vertex)
        .name(&entry_point)
        .build();

    let fragment_shader_stage_create_info = vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::FRAGMENT)
        .module(fragment)
        .name(&entry_point)
        .build();

    let bindings = [ create_binding_description() ];
    let attributes = create_attribute_description();
    let pipeline_vertex_input_state = vk::PipelineVertexInputStateCreateInfo::builder()
        .vertex_binding_descriptions(&bindings)
        .vertex_attribute_descriptions(&attributes)
        .build();
    
    let shaders_stages = vec![
        vertex_shader_stage_create_info,
        fragment_shader_stage_create_info
    ];

    VulkanShaders {
        entry: entry_point,
        shaders_stages,
        pipeline_vertex_input_state,
    }
}
