#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vec2 { pub x: f32, pub y: f32 }

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vec4 { pub r: f32, pub g: f32, pub b: f32, pub a: f32} 
pub type Color = Vec4;

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vertex {
    pub position: Vec2,
    pub color: Color
}
